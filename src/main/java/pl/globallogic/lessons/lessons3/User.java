package pl.globallogic.lessons.lessons3;

public class User {
    public String name;

    public User(String name) {this.name = name;}
    public String getName() {return name;}
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                '}';
    }
}
