package pl.globallogic.lessons.lessons3;

public class ReferenceVsPrimitiveSandbox {
    public static void main(String[] args) {
        //Primitive types
        int n1 = 5;
        int n2 = n1;
        System.out.println(n1);
        System.out.println(n2);
        n1 = 6;
        System.out.println(n1);
        System.out.println(n2);
        // 2 int vars(1st init with literal, 2nd = 1st) -> sout both -> change 1st -> sout both
        User joe1 = new User("Joe");
        User joe2 = joe1;
        System.out.println(joe1);
        System.out.println(joe2);
        joe1.name = "John";
        System.out.println(joe1);
        System.out.println(joe2);
    }
}
