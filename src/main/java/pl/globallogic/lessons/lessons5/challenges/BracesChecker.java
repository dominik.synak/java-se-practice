package pl.globallogic.lessons.lessons5.challenges;

public class BracesChecker {
    //[{}]()(()])
    public static void main(String[] args) {
        String source = "([()])";
        System.out.println(String.format("Is '%s' valid: %s", source, validate(source)));
    }

    private static boolean validate(String source) {
        int openBracesCount = 0;
        for (char brace: source.toCharArray()) {
            if(brace == '{' || brace == '[' || brace == '('){
                openBracesCount++;
            } else {
                openBracesCount--;
            }
        }
        return openBracesCount == 0;
    }
}
