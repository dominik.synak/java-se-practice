package pl.globallogic.lessons.lessons5;

import pl.globallogic.lessons.lessons3.User;

import java.util.Arrays;

public class ArraySandbox {
    public static void main(String[] args) {
        //How create an array
        String name1 = "Joe";
        String name2 = "Jane";
        String name3 = "Ivan";
        String[] names = new String[]{"Joe", "Jane", "Ivan"};
        User[] users = new User[]{new User("Joe"), new User("Jane")};
        //How to access array element
        User joe = users[0];
        joe.name = "John";
        String joeName = names[0];
        joeName = "John";
        System.out.println(users[0]);
        System.out.println(names[0]);
        //How to loop array
        for (int i = 0; i < users.length; i++) {
            System.out.println(users[i]);
        }
        //Enchanced for loop
        for(User user : users) {
            System.out.println(user);
        }
        //How to output array = Arrays.toString(...)
        System.out.println(Arrays.toString(users));
        //how to sort an array
        int[] unsortedNumbers = {6,4,6,8,9,3,12,10};
        Arrays.sort(unsortedNumbers);
        System.out.println(Arrays.toString(unsortedNumbers));
        //How to sort privitive array
        //How to search in array - Arrays.binarySearch(...)
        System.out.println(Arrays.binarySearch(unsortedNumbers, 8) == 4);
        //How to copy an array/part of array ( demo with references to same array ) = Arrays.copyOf()
        int[] peeceOfOurSortedArray = Arrays.copyOfRange(unsortedNumbers, 1, 4);
        int[] sortedCopyOfArray = Arrays.copyOf(unsortedNumbers, 16);
        String[] newNamesArray = Arrays.copyOf(names, 16);
        System.out.println(Arrays.toString(peeceOfOurSortedArray));
        System.out.println(Arrays.toString(sortedCopyOfArray));
        System.out.println(Arrays.toString(newNamesArray));
        //How to fill an array with values - Arrays.fill()
        int[] arrayOf_100 = new int[20];
        Arrays.fill(arrayOf_100, 100);
        System.out.println(Arrays.toString(arrayOf_100));
        int[] numbers1 = {1,2,3,4,5};
        int[] numbers2 = {1,2,3,4,5};
        System.out.println(Arrays.compare(numbers1, numbers2));
        System.out.println(Arrays.equals(numbers1, numbers2));
    }
}
