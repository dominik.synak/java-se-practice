package pl.globallogic.lessons.lessons1;

public class PrimitiveTypeSandbox {
    public static void main(String[] args) {
        int number = 34;
        int num1 = 128;
        int num2 = 2048;
        byte smallNumber = (byte)number;
        long bigbigNumber = 32432423;
        float floatNumber = 3.14574654F;
        char symbol = 'b';
        boolean isJavaEasy;
        String firstName= "John Doe";
        int sus = 23 + 567;
        int diff = 4546 - 5857;
        {
            int i2 = 50;
            i2++;
        }
        int idx = 1;
        int remainder = 10 % 3;
        //System.out.println(remainder);
        boolean isNum1LTENum2 = num1 == num2;
        //System.out.println(isNum1LTENum2);
        boolean alwaysTrue = true;
        //System.out.println(isNum1LTENum2 && alwaysTrue);
        String res1 = 1 > 2 ? "True" : "False";
        System.out.println(res1);
    }
}
