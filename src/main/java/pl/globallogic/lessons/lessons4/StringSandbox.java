package pl.globallogic.lessons.lessons4;

public class StringSandbox {
    public static void main(String[] args) {
        //How to create string with literals
        String greeting = "Hello";
        String longText = """
                Moze byc
                Tu
                Bardzo
                Duzo
                Tekstu
                """;
        //how to create string with new
        String greeting2 = new String("Czesc");
        //Equality check (reference, value)
        String greeting3 = "Hello";
        //System.out.println(greeting == greeting3);
        //System.out.println(greeting.equals(greeting3));
        //String immutability demo
        //greeting = greeting.toUpperCase();
        //System.out.println(greeting);
        //How to concat strings
        //greeting = greeting + greeting2;
        //System.out.println(greeting);
        //How to find out string length
        System.out.println("The lenght greeting is: " + greeting.length());
        //How to view character from a string
        for (int i = 0; i < greeting.length(); i++) {
            System.out.println(greeting.charAt(i));
        }
        String greetWithSpaces = "   Hi   ";
        //How to manipulate string case - upper, lower, trim
        System.out.println(greeting.toUpperCase());
        System.out.println(greeting.toLowerCase());
        System.out.println(greetWithSpaces);
        System.out.println(greetWithSpaces.trim());
        //How to check if string is empty, blank
        System.out.println(greeting.isBlank());
        System.out.println(greeting.isEmpty());
        //How to extract part of the string
        //How to check if string starts/ends with some other string
        System.out.println(greeting.startsWith("H"));
        System.out.println(greeting.endsWith("o"));
        System.out.println(greeting.contains("Hello"));
        //How to replace part of the string
        System.out.println(greeting.replace("Hello", "Hi"));
        //String format method - proper output
    }
}
