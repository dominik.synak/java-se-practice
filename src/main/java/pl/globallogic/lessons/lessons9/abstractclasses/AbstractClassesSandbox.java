package pl.globallogic.lessons.lessons9.abstractclasses;

public class AbstractClassesSandbox {
    //Abstract class vegicle - number of wheels,
    //implementation attributes - steeringWheels, fuelType, carcase
    //
    public static void main(String[] args) {
        Vehicle bike = new Bike("LTB", "Mountain");
        Bike realBike = (Bike) bike;
        Vehicle car = new Car("Mazda", "Diesel");
        driveTheVehicle(realBike);
        System.out.println("**********************************");
        driveTheVehicle(car);
    }

    private static void driveTheVehicle(Vehicle vehicle) {
        vehicle.drive();
    }
}
