package pl.globallogic.lessons.lessons9.abstractclasses;

public class Bike extends Vehicle{

    private final String type;
    public Bike(String producer, String type) {
        super(2, producer);
        this.type = type;
    }
    @Override
    public void drive() {
        pedal();
    }
    private void pedal() {
        System.out.println("We are pedalling...");
    }
}
