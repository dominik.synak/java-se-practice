package pl.globallogic.lessons.lessons9;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WorkingWithListMethods {
    public static void main(String[] args) {
        ArrayList<String> fruitBucket = new ArrayList<>(20);
        List<Integer> numbers = List.of(1,2,3,4,5,6);
        fruitBucket.add("Apple");
        fruitBucket.add("Orange");
        fruitBucket.add("Lemon");
        fruitBucket.add("Bananas");
        System.out.printf("# of items in our fruit bucket: %s \n", fruitBucket.size());
        System.out.printf("Is our fruit bucket empty: %s \n", fruitBucket.isEmpty());
        fruitBucket.add(2, "Grape");
        System.out.println(fruitBucket);
        List<String> smallBucket = List.of("Strawberry", "Blueberry");
        fruitBucket.addAll(smallBucket);
        System.out.println(fruitBucket);
        fruitBucket.set(3, "Dragon Fruit");
        System.out.println(fruitBucket);
        int position = 3;
        System.out.printf("On position %s in our bucket we have %s \n", position, fruitBucket.get(position-1));
        System.out.println(fruitBucket.removeAll(List.of("Bananas", "Blueberry")));
        System.out.println(fruitBucket);
        String fruitToSeek = "Grape";
        System.out.printf("%s is in %s position \n", fruitToSeek, fruitBucket.indexOf(fruitToSeek) + 1);
        String[] oldBucket = {"Apple", "Peach", "Bananas"};
        List<String> newFruitBucket = Arrays.asList(oldBucket);
        System.out.println(newFruitBucket);
        newFruitBucket.set(1, "Blueberry");
        System.out.println(newFruitBucket);
        System.out.println("Our old bucket: " + Arrays.toString(oldBucket));
    }
}
