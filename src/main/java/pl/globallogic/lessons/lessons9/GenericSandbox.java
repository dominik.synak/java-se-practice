package pl.globallogic.lessons.lessons9;

import pl.globallogic.lessons.lessons9.interfaces.Box;

public class GenericSandbox {
    //create generic class
    // parameterize it with other class
    public static void main(String[] args) {
        Box<String> smallBox = new Box<>("Jane");
        smallBox.setPayload("Jane");
        String name = smallBox.getPayload();
    }
}
