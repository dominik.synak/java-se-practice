package pl.globallogic.lessons.lessons9;

import java.util.ArrayList;
import java.util.List;

public class NongenericCollectionSandbox {

    public static void main(String[] args) {
        List<String> nonGenericStorage = new ArrayList();
        nonGenericStorage.add("Joe");
        nonGenericStorage.add("Jane");
        nonGenericStorage.add("Jack");
        for (String name :
                nonGenericStorage) {
                System.out.println(((String)name).toUpperCase());
        }
    }
}
