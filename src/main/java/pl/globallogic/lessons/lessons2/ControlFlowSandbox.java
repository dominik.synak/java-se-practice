package pl.globallogic.lessons.lessons2;

import java.net.SocketTimeoutException;
import java.util.Objects;
import java.util.Scanner;

public class ControlFlowSandbox {
    public static void main(String[] args) {
        //Scanner class to make exercises more interactive
        Scanner scanner = new Scanner(System.in);
        System.out.println("What is your name?");
        String name = scanner.next();
        System.out.println("How many times you want me to greet you?");
        int numberOfGreets = scanner.nextInt();
        String greeting = switch (name.trim()) {
            case "Chen", "Woo" -> "Nihao, " + name;
            case "Dominik" -> "Czesc, " + name;
            default -> "Hello, " + name;
        };
        /*if(Objects.equals(name, "Dominik")) {
            greeting = "Czesc, " + name;
        } else if(Objects.equals(name, "Chen")){
            greeting = "Nihao, " + name;
        }else{
            greeting = "Hello, " + name;
        }*/

        //How to manage non-linear flow - if, if-else-if
        //How to manage non-linear flow without if - more than 2 cases
        //Switch expression
        /*switch(name.trim()){
            case "Chen", "Woo": greeting = "Nihao, " + name; break;
            case "Dominik": greeting = "Czesc, " + name; break;
            default: greeting = "Hello, " + name;
        }
        System.out.println(greeting);*/
        //how to repeat some operation number of time with for loop
        for (int i = 0; i < numberOfGreets ; i++){
            System.out.println(greeting);
            if(i == 1)
                continue;
            System.out.println("Current iteration is " + i);
        }
        //how to repeat some operation with while loop
        /*int i = 0;
        do {
            System.out.println(greeting);
            i++;
        }
        while (i < numberOfGreets);*/
        //how to do at least one operaion with do-while
        //how to break out from the loop
        //how to omit iteration - continue
    }
}
