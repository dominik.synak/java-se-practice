package pl.globallogic.exercises.intermediate;

public class Ex39Polymorphism {
    public static void main(String[] args) {
        Car car = new Car(8, "Base car");
        System.out.println(car.startEngine());
        System.out.println(car.accelerate());
        System.out.println(car.brake());

        Mitsubishi mitsubishi = new Mitsubishi(6, "Outlander VRX 4WD");
        System.out.println(mitsubishi.startEngine());
        System.out.println(mitsubishi.accelerate());
        System.out.println(mitsubishi.brake());

        Ford ford = new Ford(6, "Ford Falcon");
        System.out.println(ford.startEngine());
        System.out.println(ford.accelerate());
        System.out.println(ford.brake());

        Holden holden = new Holden(6, "Holden Commodore");
        System.out.println(holden.startEngine());
        System.out.println(holden.accelerate());
        System.out.println(holden.brake());

    }
}
class Car {
    private boolean engine;
    private final int cylinders;
    private final String name;
    private int wheels;

    public Car(int cylinders, String name) {
        this.cylinders = cylinders;
        this.name = name;
    }
    public String startEngine() {
        String nameOfClass = this.getClass().getSimpleName();
        String nameOfMethod = new Object(){}.getClass().getEnclosingMethod().getName();
        return nameOfClass + " -> " + nameOfMethod;
    }
    public String accelerate() {
        String nameOfClass = this.getClass().getSimpleName();
        String nameOfMethod = new Object(){}.getClass().getEnclosingMethod().getName();
        return nameOfClass + " -> " + nameOfMethod;
    }
    public String brake() {
        String nameOfClass = this.getClass().getSimpleName();
        String nameOfMethod = new Object(){}.getClass().getEnclosingMethod().getName();
        return nameOfClass + " -> " + nameOfMethod;
    }
}
class Mitsubishi extends Car {
    public Mitsubishi(int cylinders, String name) {
        super(cylinders, name);
    }
}
class Holden extends Car {
    public Holden(int cylinders, String name) {
        super(cylinders, name);
    }
}
class Ford extends Car {
    public Ford(int cylinders, String name) {
        super(cylinders, name);
    }
}
