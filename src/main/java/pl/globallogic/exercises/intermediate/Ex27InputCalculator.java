package pl.globallogic.exercises.intermediate;

import java.util.Scanner;

public class Ex27InputCalculator {
    public static void main(String[] args) {
        inputThenPrintSumAndAverage();
    }
    private static void inputThenPrintSumAndAverage() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Print Number");
        int numbers;
        int sum = 0;
        long avg = 0;
        int i = 0;
        while(scanner.hasNextInt()) {
            numbers = scanner.nextInt();
            sum +=numbers;
            i++;
            avg = sum / i;
        }

        System.out.printf("SUM = %s AVG = %s%n",sum, avg);
    }
}
