package pl.globallogic.exercises.intermediate;

public class Ex29SumCalculator {
    public static void main(String[] args) {
        SimpleCalculator calculator = new SimpleCalculator();
        calculator.setFirstNumber(5.0);
        calculator.setSecondNumber(4);
        System.out.println("add = " + calculator.getAdditionResult());
        System.out.println("substract = " + calculator.getSubstractinonResult());
        calculator.setFirstNumber(5.25);
        calculator.setSecondNumber(0);
        System.out.println("multiply = " + calculator.getMultiplicationResult());
        System.out.println("divide = " + calculator.getDivisionResult());
    }
}
class SimpleCalculator {
    private double firstNumber;
    private double secondNumber;
    public double getFirstNumber() {
        return firstNumber;
    }
    public double getSecondNumber() {
        return secondNumber;
    }
    public void setFirstNumber(double firstNumber) {
        this.firstNumber = firstNumber;
    }
    public void setSecondNumber(double secondNumber) {
        this.secondNumber = secondNumber;
    }
    public double getAdditionResult() {
        return getFirstNumber() + getSecondNumber();
    }
    public double getSubstractinonResult() {
        return getFirstNumber() - getSecondNumber();
    }
    public double getMultiplicationResult() {
        return getFirstNumber() * getSecondNumber();
    }
    public double getDivisionResult() {
        if(secondNumber == 0) return 0;
        return getFirstNumber() / getSecondNumber();
    }
}
