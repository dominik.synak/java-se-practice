package pl.globallogic.exercises.intermediate;

public class Ex16NumberPalindrome {
    public static void main(String[] args) {
        System.out.println(isPalindrome(-1221));
        System.out.println(isPalindrome(707));
        System.out.println(isPalindrome(11212));
    }
    private static boolean isPalindrome(int number) {
        int rev = 0;
        int temp = number;
        int digit;
        while(temp !=0) {
            digit = temp%10;
            rev = rev * 10 + digit;
            temp /= 10;
        }
        return number == rev;
    }
}
