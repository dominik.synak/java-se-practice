package pl.globallogic.exercises.intermediate;

public class Ex25FlourPackProblem {
    public static void main(String[] args) {
        System.out.println(canPack(1,0,4));
        System.out.println(canPack(1,0,5));
        System.out.println(canPack(0,5,4));
        System.out.println(canPack(2,2,11));
        System.out.println(canPack(-3,2,12));
    }
    private static boolean canPack(int bigCount, int smallCount, int goal) {
        if (bigCount < 0 || smallCount < 0 || goal < 0) return false;
        int totalCount = (bigCount * 5) + smallCount;
        if ((bigCount*5) == goal || smallCount == goal || totalCount == goal) return true;
        int diff = totalCount - goal;
        if (diff < 0) return false;
        int remainder = goal % 5;
        int bigCountNeeded = (goal - remainder) / 5;
        if (bigCount >= bigCountNeeded) return remainder <= smallCount;
        else {
            int neededForGoal = ((bigCountNeeded - bigCount) * 5) + remainder;
            return neededForGoal <= smallCount;
        }
    }
}
