package pl.globallogic.exercises.intermediate;

public class Ex18EvenDigitSum {
    public static void main(String[] args) {
        System.out.println(getEvenDigitSum(123456789) == 20);
        System.out.println(getEvenDigitSum(252) == 4);
        System.out.println(getEvenDigitSum(-22) == -1);
    }
    private static int getEvenDigitSum(int number) {
        if(number < 0) return -1;
        int temp;
        int sum = 0;
        while(number > 0) {
            temp = number%10;
            if(temp%2 == 0) {
                sum += temp;
            }
            number = number/10;
        }
        return sum;
    }
}
