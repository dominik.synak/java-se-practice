package pl.globallogic.exercises.intermediate.Ex48Interface;
import java.util.List;
import java.util.ArrayList;

public interface ISaveable {
    // write code here
    List<String> write();

    void read(List<String> lst);
}
