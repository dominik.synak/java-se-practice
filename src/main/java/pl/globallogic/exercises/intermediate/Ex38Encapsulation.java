package pl.globallogic.exercises.intermediate;

public class Ex38Encapsulation {
    public static void main(String[] args) {
        Printer printer = new Printer(50, true);
        System.out.println(printer.addToner(50));
        System.out.println("initial page count = " +printer.getPagesPrinted());
        int pagesPrinted = printer.printPages(4);
        System.out.println("Pages printed was " + pagesPrinted +" new total print count for printer = " +printer.getPagesPrinted());
        pagesPrinted = printer.printPages(2);
        System.out.println("Pages printed was " + pagesPrinted +" new total print count for printer = " +printer.getPagesPrinted());

    }
}
class Printer {
    private final int tonerLevel;
    private int pagesPrinted;
    private final boolean duplex;

    public Printer(int tonerLevel, boolean duplex) {
        if (tonerLevel < 0 || tonerLevel > 100) tonerLevel = -1;
        this.tonerLevel = tonerLevel;
        this.duplex = duplex;
    }
    public int addToner(int tonerAmount) {
        if (tonerAmount > 0 && tonerAmount <= 100) {
            if ((tonerLevel + tonerAmount) > 100) return -1;
            else return tonerLevel + tonerAmount;
        }
        else return -1;
    }
    public int printPages(int pages) {
        int pagesToPrint;
        if(duplex) {
            System.out.println("Printing in duplex mode");
            int temp;
            temp = pages/2 + pages%2;
            pagesToPrint = temp;
        }
        else {
            System.out.println("Printing without duplex mode");
            pagesToPrint = pages;
        }
        this.pagesPrinted += pagesToPrint;
        return pagesToPrint;
    }
    public int getPagesPrinted() {
        return this.pagesPrinted;
    }


}

