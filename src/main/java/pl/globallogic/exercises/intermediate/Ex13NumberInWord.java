package pl.globallogic.exercises.intermediate;

import java.util.Scanner;

public class Ex13NumberInWord {
    public static void main(String[] args) {
        Scanner scanner = new Scanner((System.in));
        System.out.println("Input Number: ");
        int number = scanner.nextInt();
        System.out.println(printNumberInWord(number));
    }
    private static String printNumberInWord(int number) {
        return switch (number) {
            case 0 -> "ZERO";
            case 1 -> "ONE";
            case 2 -> "TWO";
            case 3 -> "THREE";
            case 4 -> "FOUR";
            case 5 -> "FIVE";
            case 6 -> "SIX";
            case 7 -> "SEVEN";
            case 8 -> "EIGHT";
            case 9 -> "NINE";
            default -> "OTHER";
        };
    }
}
