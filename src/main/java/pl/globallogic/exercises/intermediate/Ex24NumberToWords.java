package pl.globallogic.exercises.intermediate;

public class Ex24NumberToWords {
    public static void main(String[] args) {
        System.out.println(getDigitCount(0) == 1);
        System.out.println(getDigitCount(123) == 3);
        System.out.println(getDigitCount(-12) == -1);
        System.out.println(getDigitCount(5200) == 4);

        System.out.println(reverse(-121) == -121);
        System.out.println(reverse(1212) == 2121);
        System.out.println(reverse(1234) == 4321);
        System.out.println(reverse(100) == 1);

        numberToWords(123);
        numberToWords(1010);
        numberToWords(1000);
        numberToWords(-12);
    }
    private static void numberToWords(int number) {
        int digitCount = getDigitCount(number);
        int revNumber = reverse(number);
        int temp = revNumber;
        if (revNumber < 0 ) System.out.println("Invalid Value");
        int digit;
        while (revNumber !=0) {
            digit = revNumber%10;
            switch (digit) {
                case 0 -> System.out.println("ZERO");
                case 1 -> System.out.println("ONE");
                case 2 -> System.out.println("TWO");
                case 3 -> System.out.println("THREE");
                case 4 -> System.out.println("FOUR");
                case 5 -> System.out.println("FIVE");
                case 6 -> System.out.println("SIX");
                case 7 -> System.out.println("SEVEN");
                case 8 -> System.out.println("EIGHT");
                case 9 -> System.out.println("NINE");
            }
            revNumber /=10;
            }
        for (int i = getDigitCount(temp); i < digitCount; i++) {
            System.out.println("ZERO");
        }
    }
    private static int reverse (int number) {
        int rev = 0;
        int digit;
        while(number != 0) {
            digit = number%10;
            rev = rev * 10 + digit;
            number /= 10;
        }
        return rev;
    }
    private static int getDigitCount(int number) {
        if(number < 0) return -1;
        if(number == 0) return 1;
        int i = 0;
        while(number != 0) {
            number /= 10;
            i++;
        }
        return i;
    }
}
