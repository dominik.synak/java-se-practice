package pl.globallogic.exercises.intermediate;

public class Ex30Person {
    public static void main(String[] args) {
        Person person = new Person();
        person.setFirstName("");
        person.setLastName("");
        person.setAge(10);
        System.out.println("fullName= " + person.getFullName());
        System.out.println("teen= " + person.isTeen());
        person.setFirstName("John");
        person.setAge(18);
        System.out.println("fullName= " + person.getFullName());
        System.out.println("teen= " + person.isTeen());
        person.setLastName("Smith");
        System.out.println("fullName= " + person.getFullName());
    }
}
class Person {
    private String firstName;
    private String lastName;
    private int age;
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public int getAge() {
        return age;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public void setAge(int age) {
        if(age < 0 || age > 100) this.age = 0;
        else this.age = age;
    }
    public boolean isTeen() {
        return getAge() > 12 && getAge() < 20;
    }
    public String getFullName() {
        if(getFirstName().isEmpty() && getLastName().isEmpty()) return "";
        if(getLastName().isEmpty()) return getFirstName();
        if(getFirstName().isEmpty()) return getLastName();
        return getFirstName() + " " + getLastName();
    }
}
