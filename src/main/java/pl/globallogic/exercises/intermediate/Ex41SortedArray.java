package pl.globallogic.exercises.intermediate;

import java.util.Scanner;

public class Ex41SortedArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter size of array: ");
        int sizeOfArray = scanner.nextInt();
        int[] arrayOfIntegers = getIntegers(sizeOfArray);
        printArray(arrayOfIntegers);
        int[] sortedIntegers = sortIntegers(arrayOfIntegers);
        System.out.println("Sorted array: ");
        for (int i = 0; i < sortedIntegers.length; i++) {
            System.out.printf("Element %s contents %s \n", i, sortedIntegers[i]);
        }
    }
    public static int[] getIntegers(int sizeOfArray) {
        return new int[sizeOfArray];
    }
    public static void printArray(int[] arrayOfIntegers) {
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < arrayOfIntegers.length; i++) {
            System.out.printf("Enter %s number: ", i + 1);
            arrayOfIntegers[i] = scanner.nextInt();
        }
        System.out.println("Unsorted array: ");
        for (int i = 0; i < arrayOfIntegers.length; i++) {
            System.out.printf("Element %s contents %s \n", i, arrayOfIntegers[i]);
        }
    }
    public static int[] sortIntegers(int[] unsortedArrayOfIntegers) {
        for (int i = 0; i < unsortedArrayOfIntegers.length - 1; i++) {
            for (int j = 0; j < unsortedArrayOfIntegers.length - 1; j++) {
                if(unsortedArrayOfIntegers[j] < unsortedArrayOfIntegers[j + 1]) {
                    int temp = unsortedArrayOfIntegers[j];
                    unsortedArrayOfIntegers[j] = unsortedArrayOfIntegers[j + 1];
                    unsortedArrayOfIntegers[j + 1] = temp;
                }
            }
        }
        return unsortedArrayOfIntegers;
    }
}
