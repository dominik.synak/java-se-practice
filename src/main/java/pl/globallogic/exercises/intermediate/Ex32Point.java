package pl.globallogic.exercises.intermediate;

public class Ex32Point {
    public static void main(String[] args) {
        Point first = new Point(6, 5);
        Point second = new Point(3, 1);
        System.out.println("distance(0,0)= " + first.distance());
        System.out.println("distance(second)= " + first.distance(second));
        System.out.println("distance(2,2)= " + first.distance(2,2));
        Point point = new Point();
        System.out.println("distance()= " + point.distance());
    }
}
class Point {
    private int x;
    private int y;
    public Point() {
    }
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public int getX() {
        return this.x;
    }
    public int getY() {
        return this.y;
    }
    public void setX(int x) {
        this.x = x;
    }
    public void setY(int y) {
        this.y = y;
    }
    public double distance() {
        Point a = new Point(getX(), getY());
        Point b = new Point(0, 0);
        return Math.sqrt((b.getX() - a.getX()) * (b.getX() -
                a.getX()) + (b.getY() - a.getY()) *
                (b.getY() - a.getY()));
    }
    public double distance(int x, int y) {
        Point a = new Point(getX(), getY());
        Point b = new Point(x, y);
        return Math.sqrt((b.getX() - a.getX()) * (b.getX() -
                a.getX()) + (b.getY() - a.getY()) *
                (b.getY() - a.getY()));
    }
    public double distance(Point another) {
        Point a = new Point(getX(), getY());
        Point b = new Point(another.getX(), another.getY());
        return Math.sqrt((b.getX() - a.getX()) * (b.getX() -
                a.getX()) + (b.getY() - a.getY()) *
                (b.getY() - a.getY()));
    }
}
