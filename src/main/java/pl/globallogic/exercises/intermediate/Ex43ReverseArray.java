package pl.globallogic.exercises.intermediate;

import java.util.Arrays;

public class Ex43ReverseArray {
    public static void main(String[] args) {
        int[] arrayOfNumbers = new int[] {3,2,1,4,5,6};
        System.out.println("Array: " + Arrays.toString(arrayOfNumbers));
        System.out.println("Reversed array: " + Arrays.toString(reverse(arrayOfNumbers)));
    }
    public static int[] reverse(int[] anArray) {
        int lenghtOfArray = anArray.length - 1;
        for (int i = 0; i < (anArray.length)/2; i++) {
            int temp = anArray[lenghtOfArray];
            anArray[lenghtOfArray] = anArray[i];
            anArray[i] = temp;
            lenghtOfArray--;
        }
        return anArray;
    }
}
