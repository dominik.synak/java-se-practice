package pl.globallogic.exercises.intermediate;

public class Ex19SharedDigit {
    public static void main(String[] args) {
        System.out.println(hasSharedDigit(12, 23));
        System.out.println(hasSharedDigit(9, 99));
        System.out.println(hasSharedDigit(15, 55));
    }
    private static boolean hasSharedDigit(int first, int second) {
        if(first < 10 || first > 99 || second < 10 || second > 99) return false;
        int leftFirst = first/10;
        int rightFirst = first%10;
        int leftSecond = second/10;
        int rightSecond = second%10;
        return leftFirst == leftSecond || leftFirst == rightSecond || rightFirst == leftSecond || rightFirst == rightSecond;
    }
}
