package pl.globallogic.exercises.intermediate;

import java.util.Scanner;

public class Ex42MinimumElement {
    public static void main(String[] args) {
        int sizeOfArray = readInteger();
        int[] arrayOfIntegers = readElements(sizeOfArray);
        System.out.println("Minimum value in array is: " + findMin(arrayOfIntegers));
    }
    public static int readInteger() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number of elements: ");
        return scanner.nextInt();
    }
    public static int[] readElements(int num) {
        Scanner scanner = new Scanner(System.in);
        int[] anArray = new int[num];
        for (int i = 0; i < anArray.length; i++) {
            System.out.print("Enter element: ");
            anArray[i] = scanner.nextInt();
        }
        return anArray;
    }
    public static int findMin(int[] array) {
        int minInt = array[0];
        for (int num : array) {
            if (num < minInt) minInt = num;
        }
        return minInt;
    }
}
