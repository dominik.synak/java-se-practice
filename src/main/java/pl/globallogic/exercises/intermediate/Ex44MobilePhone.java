package pl.globallogic.exercises.intermediate;

import java.util.ArrayList;

public class Ex44MobilePhone {
    public static void main(String[] args) {
        Contact Bob = new Contact("Bob", "31415926");
        Contact Alice = new Contact("Alice", "16180339");
        Contact Tom = new Contact("Tom", "11235813");
        Contact Jane = new Contact("Jane","23571113");
        Contact Joe = new Contact("Joe","24345345");
        MobilePhone phone = new MobilePhone("123456789");
        phone.addNewContact(Bob);
        phone.addNewContact(Alice);
        phone.addNewContact(Tom);
        phone.addNewContact(Jane);
        phone.printContacts();
        //Contact exist
        System.out.println(phone.addNewContact(Jane) == false);
        //remove contact
        System.out.println(phone.removeContact(Bob) == true);
        phone.printContacts();
        //remove - doesn't exist
        System.out.println(phone.removeContact(new Contact("bob", "232")) == false);
        //update contact
        System.out.println(phone.updateContact(Jane, Joe) == true);
        phone.printContacts();
        //query contact
        System.out.println(phone.queryContact("Tom") == Tom);
    }
}
class MobilePhone {
    private final String myNumber;
    private ArrayList<Contact> myContacts;

    public MobilePhone(String myNumber) {
        this.myNumber = myNumber;
        this.myContacts = new ArrayList<Contact>();
    }
    public boolean addNewContact(Contact contact) {
        if(findContact(contact.getName()) >= 0) {
            System.out.println("Contact already exist.");
            return false;
        }
        myContacts.add(contact);
        return true;
    }
    public boolean updateContact(Contact contactOld, Contact contactNew) {
        int position = findContact(contactOld);
        if(position < 0) {
            System.out.println(contactOld.getName() + " contact was not found");
            return false;
        } else if (findContact(contactNew.getName()) != -1) {
            System.out.printf("Contact %s exist. Update was not successful. \n", contactNew.getName());
            return false;
        }
        this.myContacts.set(position,contactNew);
        System.out.printf("%s was replaced with %s \n", contactOld.getName(), contactNew.getName());
        return true;
    }
    public boolean removeContact(Contact contact) {
        int position = findContact(contact);
        if(position < 0) {
            System.out.println(contact.getName() + " was not found.");
            return false;
        }
        this.myContacts.remove(position);
        System.out.println(contact.getName() + " was deleted.");
        return true;
    }
    public int findContact(Contact contact) {
        if(myContacts.contains(contact)) return myContacts.indexOf(contact);
        else return -1;
    }
    public int findContact(String contactName) {
        for (int i = 0; i < this.myContacts.size(); i++) {
            Contact contact = this.myContacts.get(i);
            if(contact.getName().equals(contactName)) return i;
        }
        return -1;
    }
    public Contact queryContact(String contactName) {
        int position = findContact(contactName);
        if(position >= 0) {
            return this.myContacts.get(position);
        }
        return null;
    }
    public void printContacts() {
        System.out.println("Contact list: ");
        for (int i = 0; i < this.myContacts.size(); i++) {
            System.out.printf("%s. %s -> %s \n", (i+1), this.myContacts.get(i).getName(), this.myContacts.get(i).getPhoneNumber());
        }
    }
}
class Contact {
    private final String name;
    private final String phoneNumber;

    public Contact(String name, String phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
    public static Contact createContact(String name, String phoneNumber) {
        return new Contact(name, phoneNumber);
    }
}
