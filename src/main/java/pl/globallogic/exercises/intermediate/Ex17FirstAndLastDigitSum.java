package pl.globallogic.exercises.intermediate;

public class Ex17FirstAndLastDigitSum {
    public static void main(String[] args) {
        System.out.println(sumFirstAndLastDigit(252) == 4);
        System.out.println(sumFirstAndLastDigit(257) == 9);
        System.out.println(sumFirstAndLastDigit(0) == 0);
        System.out.println(sumFirstAndLastDigit(5) == 10);
        System.out.println(sumFirstAndLastDigit(-10) == -1);
    }
    private static int sumFirstAndLastDigit(int number) {
        if(number < 0) return -1;
        int digits = (int)Math.log10(number);
        int firstDigit = (int)(number / Math.pow(10, digits));
        int lastDigit = number%10;
        return firstDigit + lastDigit;
    }
}
