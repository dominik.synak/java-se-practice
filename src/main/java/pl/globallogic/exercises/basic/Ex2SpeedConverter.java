package pl.globallogic.exercises.basic;

public class Ex2SpeedConverter {
    public static void main(String[] args) {
        System.out.println(toMilesPerHour(1.5));
        System.out.println(toMilesPerHour(10.25));
        System.out.println(toMilesPerHour(-5.6));
        System.out.println(toMilesPerHour(25.42));
        System.out.println(toMilesPerHour(75.114));

        printConversion(1.5);
        printConversion(10.25);
        printConversion(-5.6);
        printConversion(25.42);
        printConversion(75.114);

    }
    private static long toMilesPerHour(double kilometersPerHour){
        if(kilometersPerHour < 0){ return -1; }
        else {
            double multiplier = 0.62137119;
            double milesPerHour = Math.round(kilometersPerHour*multiplier);
            return (long) milesPerHour;
        }
    }
    private static void printConversion(double kilometersPerHour){
        if(kilometersPerHour < 0) {
            System.out.println("Invalid Value");
        }
        else{
            double multiplier = 0.62137119;
            double milesPerHour = Math.round(kilometersPerHour*multiplier);
            System.out.println(kilometersPerHour + " km/h = " + (long)milesPerHour + " mi/h");
        }
    }
}
