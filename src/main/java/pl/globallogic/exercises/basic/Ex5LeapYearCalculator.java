package pl.globallogic.exercises.basic;

public class Ex5LeapYearCalculator {
    public static void main(String[] args) {
        System.out.println(isLeapYear(-1600));
        System.out.println(isLeapYear(1600));
        System.out.println(isLeapYear(2017));
        System.out.println(isLeapYear(2000));
    }
    public static boolean isLeapYear(int Year) {
        if(Year >= 1 && Year <=9999 ) {
            if(Year%4 == 0) {
                if(Year%100 == 0) {
                    return Year % 400 == 0;
                }
                else return true;
            }
            else return false;
        }
        else return false;
    }
}
