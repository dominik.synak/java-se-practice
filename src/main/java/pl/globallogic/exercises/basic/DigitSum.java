package pl.globallogic.exercises.basic;

import java.util.Scanner;

public class DigitSum {
    //suma cyfr liczby
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please pass a number: ");
        int number = scanner.nextInt();
        System.out.println(sumOfDigits(number));
    }
    private static int sumOfDigits(int number) {
        int temp;
        int sumOfDigits = 0;
        if(number < 0) temp = -number;
        else {
            temp = number;
        }
        if(temp == 0) return 0;
        while (temp > 0) {
            sumOfDigits += temp%10;
            temp/=10;
        }
        return sumOfDigits;
    }
}
