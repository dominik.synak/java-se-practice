package pl.globallogic.exercises.basic;

import java.util.Scanner;

public class DigitCounter {
    /*
    * Count the digits in the rational number
    * 13456 - 5
    * 89 - 2
    * -45 - -1
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the numer to count digits:");
        int number = scanner.nextInt();
        System.out.println("Digit count for " + number + " is equal to " +numberOfDigits(number));
        /*System.out.println(numberOfDigits(13456) == 5);
        System.out.println(numberOfDigits(89) == 2);
        System.out.println(numberOfDigits(-45) == -1);*/
    }
    private static int numberOfDigits(int targetNumber) {
        if(targetNumber < 0) return -1;
        int counter = 0;
        while(targetNumber > 0) {
            targetNumber = targetNumber / 10;
            counter++;
        }
        return counter;
    }
}
